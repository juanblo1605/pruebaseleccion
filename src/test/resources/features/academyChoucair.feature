#Autor: Juan Pablo Hoyos Salazar
  @stories
  Feature: Academy Choucair
    As a user, I want to learn how to automate in screamplay at the Choucair Academy with the automation course
  @scenario1
  Scenario: Search for a automation course
    Given than Juan Pablo wants to learn automation at the Academy Choucair
    When he search for the course Prueba Tecnica Analista Bancolombia on the Choucair Academy platform
    Then he finds the course called resources Recursos Automatizacion Bancolombia
